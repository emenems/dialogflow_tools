# Dialogflow tools
Set of own functions to interact with dialogflow

## Functions
* **get_intent**: get bot response providing text, botID, and sessionID
* **correct_message_datetime**: correct dialogflow time (9:30 is by dialogflow parsed as 9:30 PM instead of AM) and date (normally does not recognize EU date format of DD.MM.)
* **translate_gcp**: translate text using google cloud platform/google translate
* **to_kommunicate**: convert dialgoflow message to [kommunicate.io](https://docs.kommunicate.io/docs/bot-custom-integration) format
* **from_kommunicate**: convert [kommunicate.io](https://docs.kommunicate.io/docs/bot-custom-integration) dictionary to own format used in this package (to have unified formatting independent of bot API)
* **to_custom_dtse**: convert dialogflow message to custom format used by DTSE CZ front end
* **from_custom_dtse**: convert custom DTSE CZ front end request to own format used in this package (to have unified formatting independent of bot API)
* **create_intent_json**: Create dict/JSON intent for dialogflow import to dialogflow
* **create_intent**: create intent directly in dialogflow
* **delete_intent**: delete intent from you agent
* **find_intent**: function allowing to (1) find intent ID based on display name, (2) get the intent as dictionary, (3) back up the intent as JSON
* **query_intents**: list all available intents for the given bot/agent
* **query_intent**: will get all info for given Intent ID
* **update_intent**: update existing intent providing training phrases, messages, or display name for given Intent ID
* **query_training_phrases** query all training phrases for Intent ID

### Installation
* Optional: create python [virtual environment](https://virtualenv.pypa.io/en/latest/userguide.html)
	* `virtualenv SOME-DIRECTORY`
	* `source /path/to/SOME-DIRECTORY/bin/activate`
	* to stop env simpy call `deactivate`
* install dependencies:
	* go to folder where dialogflow_tools is located (not to env SOME-DIRECTORY)
	* `pip install -r requirements.txt`
* install package locally (for current user)
	* go to folder where dialogflow_tools is located (where setup.py file is located)
	* `pip install --user .`

### Test
* To carry out [unit-test](https://docs.pytest.org/en/latest/), just go to the root folder (where setup.py is located) and run `pytest` from shell
	* to run all tests, make sure `dialogflow_tools/tests/_aux_/dialogflow_credentials_valid.json` exists

### Usage
```python
import os
from dialogflow_tools import get_intent
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"dialogflow_tools/tests/_aux_/dialogflow_credentials_valid.json"

# get dialogflow bot response
text_input = "Please reserve me a table for 9:30 on 23.8."
response_out = get_intent(text_input, "demochatbots", "unique")

# translate message (will use different credentials)
from dialogflow_tools import translate_gcp
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"dialogflow_tools/tests/_aux_/dialogflow_credentials_translate_valid.json"
out = translate_gcp(["Ahoj, preloz to prosim"],language_list = ["sk","cz"])
```
