from setuptools import setup, find_packages

setup(
    name="dialogflow_tools",
    version="0.2.3",
    description="dialogflow_tools",
    author="Michal Mikolaj",
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        'google-cloud-translate==3.6.1',
        'google-cloud-dialogflow==2.10.0',
        'protobuf==3.19.1',
        'numpy',
        'scipy'
    ],
)
