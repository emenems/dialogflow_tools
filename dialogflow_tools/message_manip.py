import warnings
from datetime import datetime
try:
    from calendar_tools import parse_time
    from calendar_tools import parse_date
    from calendar_tools import from_gcp_time
    from calendar_tools import to_gcp_time
except ModuleNotFoundError:
    warnings.warn("calendar_tools module is not found")


def correct_message_datetime(response_in, time_field: str = "time",date_field: str = "date"):
    """
    Correct message time caused by dialogflow conversion, e.g., 9:30 is converted 21:30 (=assumes PM by default)cc
    Date is converted from, e.g., 9.2. to 

    ## Input:
    * response_in: dialgoflow response dictionary: https://developers.google.com/assistant/actions/build/json/dialogflow-webhook-json
    * time_filed: field containing time (entity name)
    * date_field: field containing data (entity name)

    ## Output
    * corrected dictionary (need to convert to Message for use in dialogflow)

    ## Example
    ```
    import os
    import dialogflow_v2 as dialogflow
    import google.protobuf as gpf
    from dialogflow_tools import correct_message_datetime
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"d:/projekt/credentials/demochatbots-1efee945dc06.json"

    session_client = dialogflow.SessionsClient()
    session = session_client.session_path("demochatbots", "123930384")
    print('Session path: {}\n'.format(session))
    text_input = dialogflow.types.TextInput(
                text="Please reserve me a table for 9:30 on 23.8.", language_code="en")
    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent(
                session=session, query_input=query_input)
    response_dict = gpf.json_format.MessageToDict(response)
    response_new = correct_message_datetime(response_dict,date_field="eu_date")
    # response_new = gpf.json_format.ParseDict(response_new,response)
    ```
    """
    response_out = response_in.copy()
    # run for all outputContext entries
    for i in range(len(response_out["queryResult"]["outputContexts"])):
        temp = response_out["queryResult"]["outputContexts"][i]["parameters"]
        # correct time
        if time_field in temp.keys():
            if temp[time_field] != "":
                dt_orig = parse_time(temp[time_field+".original"])
                dt = from_gcp_time(temp[time_field])
                if dt.hour != dt_orig.hour or dt.minute != dt_orig.minute:
                    response_out["queryResult"]["outputContexts"][i]["parameters"][time_field] = to_gcp_time(datetime(dt.year,
                                                                                                              dt.month,
                                                                                                              dt.day,
                                                                                                              dt_orig.hour,
                                                                                                              dt.minute,
                                                                                                              dt.second))
        # correct date
        if date_field in temp.keys():
            if temp[date_field] != "":
                response_out["queryResult"]["outputContexts"][i]["parameters"][date_field] = to_gcp_time(parse_date(
                                                                                                          temp[date_field+".original"]))
    return response_out
