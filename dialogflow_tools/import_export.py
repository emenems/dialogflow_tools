import json
from google.cloud import dialogflow
from .message_io import dialogflow_dict
from google.protobuf.field_mask_pb2 import FieldMask


def create_intent_json(display_name: str,
                       user_asks: list,
                       bot_replies: list,
                       output_file: str = None,
                       is_fallback: bool = False) -> dict:
    """Create dict/JSON intent for dialogflow import to dialogflow

    Parameters
    ----------
    display_name: name of the intent
    user_asks: list of questions that user would ask
    bot_replies: what should the bot reply
    output_file: if the output should be written to JSON file. Set to None for no output file

    Returns
    -------
    intent as dictionary

    Examples
    ```
    user_asks = ["Test1","Test2"]
    bot_replies = [["Row1_option1","Row1_option2"],"Row2"]
    # without writing
    out = create_intent_json("First test",user_asks,bot_replies)

    # no reply & write to file
    out = create_intent_json("Forms - Test Export",
                            ["I want to test the export form",
                             "How can I fill the test form",
                             "Where do I find test forms"],
                             None,
                             "forms_test_export.json")
    ```
    """
    out = template_intent_json()

    if user_asks is None or user_asks == []:
        user_says = {"isTemplate": False, "data": [{"text": "", "userDefined": False}], "count": 0, "updated": None}
    else:
        user_says = []
        for question in user_asks:
            user_says.append({"isTemplate":False,
                            "data":[{"text":question, "userDefined":False}],
                            "count":0,
                            "updated":None})

    if bot_replies is None or bot_replies == []:
        replies = [{"type": "message", "condition": "", "speech": []}]
    else:
        replies = []
        for reply in bot_replies:
            if not isinstance(reply, list):
                reply = [reply]
            replies.append({'type': 'message', 'condition': '', 'speech': [i for i in reply]})

    out["userSays"] = user_says
    out["responses"][0]["messages"] = replies
    out["name"] = display_name
    out["fallbackIntent"] = is_fallback

    if output_file is not None:
        with open(output_file, 'w') as fid:
            json.dump(out, fid, indent=4)

    return out


def template_intent_json() -> dict:
    """Aux function generating intent dictionary/JSON"""
    out = {
            'name': 'display_name',
            'auto': True,
            'condition': '',
            'conditionalFollowupEvents': [],
            'conditionalResponses': [],
            'context': [],
            'contexts': [],
            'endInteraction': False,
            'events': [],
            'fallbackIntent': False,
            'liveAgentHandoff': False,
            'parentId': None,
            'followUpIntents': [],
            'priority': 500000,
            'responses': [{
                'action': '',
                'affectedContexts': [],
                'parameters': [],
                'defaultResponsePlatforms': {},
                'messages': [{
                    'type': 'message',
                    'condition': '',
                    'speech': ['text_response1', 'text_response2']
                }, {
                    'type': 'message',
                    'condition': '',
                    'speech': ['text_response3']
                }],
                'resetContexts': False
            }],
            'rootParentId': None,
            'templates': [],
            'userSays': [{
                'isTemplate': False,
                'data': [{
                    'text': 'training_phrase1',
                    'userDefined': False
                }],
                'count': 0,
                'updated': None
            }, {
                'isTemplate': False,
                'data': [{
                    'text': 'training_phrase2',
                    'userDefined': False
                }],
                'count': 0,
                'updated': None
            }],
            'webhookForSlotFilling': False,
            'webhookUsed': False
        }
    return out



def query_intents(bot_id: str, to_dict: bool = True) -> dict:
    """Query all intents for given Bot/Project ID (assuming GOOGLE_APPLICATION_CREDENTIALS is set as
      .env variable)

    See https://cloud.google.com/dialogflow/es/docs/how/manage-intents for details

    ## Parameters:
    --------------
    * bot_id: Google Project ID (not dialogflow chatbot name!)
    * to_dict: True if the output should be converted to dictionaries

    ## Returns:
    -----------
    * If 'to_dict' is True: dictionary with all intents and unique intent ID (not name) as keys
    * If 'to_dict' is False: original dialogflow object

    ## Example:
    ----------
    ```
    import os
    from dialogflow_tools import query_intents

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"/references/credentials/dialogflow_chuck3.json"

    bot_id = "chuckcopy-jrbd"

    intents = query_intents(bot_id)
    ```
    """
    # Get all intents as dialogflow object
    intents_client = dialogflow.IntentsClient()
    parent = dialogflow.AgentsClient.agent_path(bot_id)
    intents = intents_client.list_intents(request={"parent": parent})
    if to_dict == False:
        return intents
    else:
        # Convert to dict
        out = dict()
        for intent in intents:
            out[intent.name] = dialogflow_dict(intent)
        return out



def find_intent(
        bot_id: str,
        intent_name: str,
        use_display_name: bool = True,
        return_intent: bool = False,
        output_file: str = None
    ) -> str:
    """Find intent given its Intent Display Name or Intent ID

    Parameters:
    -----------
    * bot_id: bot/project ID or dictionary containing all intents (returned by query_intents)
    * intent_name: name of ID of the intent (see 'use_display_name' parameter). If ID, only the
        name, not the full path is expected
    * use_display_name: True if display name (may not be unique), and Fals if intent ID (unique)
    * return_intent: set to True if you want to return the whole intent dictionary, not it's ID
    * output_file: if the output should be written to JSON file. Set to None for no output file

    Returns:
    --------
    * If `return_intent` is False (default), unique ID of the intent (full). If `return_intent`
        True, will return dictionary. Will be None if not found.
    """
    # query all intents if not provided
    if isinstance(bot_id, str):
        intents = query_intents(bot_id, to_dict=True)
    else:
        intents = bot_id
    # Find the intent
    id = None
    for k,i in intents.items():
        compare_to = i["displayName"] if use_display_name == True else k.split("/")[-1]
        if compare_to == intent_name:
            if output_file is not None:
                with open(output_file, 'w') as fid:
                    json.dump(i, fid, indent=4)
            return i if return_intent == True else k
    return id if return_intent == False else {}



def delete_intent(intent_id):
    """Delete the selected intent using full intent ID (meaning with path as retured by find_intent)

    See https://cloud.google.com/dialogflow/es/docs/how/manage-intents for source

    Parameters:
    --------------
    * intent_id: intent identifier (use find_intent to find it)

    Returns:
    --------
    True if deleted (may take some time to see it in GUI), False if not deleted

    ## Example:
    ----------
    ```
    import os
    from dialogflow_tools import query_intents

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"/references/credentials/dialogflow_chuck3.json"

    bot_id = "chuckcopy-jrbd"

    intent_id = find_intent(bot_id,"Test For Delete",use_display_name=True)

    delete_intent(intent_id)
    ```
    """
    if intent_id is None:
        return False
    else:
        try:
            intents_client = dialogflow.IntentsClient()
            intents_client.delete_intent(request={"name": intent_id})
        except:
            return False
        return True


def create_intent(
        bot_id: str,
        display_name: str,
        user_asks: list,
        bot_replies: list,
        is_fallback: bool = False
    ) -> dict:
    """Create intent directly in dialogflow via API

    See https://cloud.google.com/dialogflow/es/docs/how/manage-intents for source

    Parameters
    ----------
    bot_id: ID of the Bot/Project
    display_name: name of the intent
    user_asks: list of questions that user would ask
    bot_replies: what should the bot reply. Use multiple lists to allow for random choice of
        replies, see example
    is_fallback: if given intent is a Fallback intent (False by default)

    Returns
    -------
    created intent as dictionary

    Examples
    ```
    bot_id = "chuckcopy-jrbd"
    user_asks = ["Test1","Test2"]
    # The first replie will allow for random choice. The 2nd row just adds a new line to the 1st one
    bot_replies = [["Row1_option1","Row1_option2"],"Row2"]
    display_name = "TEST to be deleted"

    out = create_intent(bot_id,display_name,user_asks,bot_replies)
    ```
    """
    intents_client = dialogflow.IntentsClient()
    parent = dialogflow.AgentsClient.agent_path(bot_id)

    training_phrases = []
    for training_phrases_part in user_asks:
        part = dialogflow.Intent.TrainingPhrase.Part(text=training_phrases_part)
        # Here we create a new training phrase for each provided part.
        training_phrase = dialogflow.Intent.TrainingPhrase(parts=[part])
        training_phrases.append(training_phrase)

    messages = []
    for i in bot_replies:
        text = dialogflow.Intent.Message.Text(text=i if isinstance(i,list) else [i])
        messages.append(dialogflow.Intent.Message(text=text))

    intent = dialogflow.Intent(
        display_name=display_name,
        training_phrases=training_phrases,
        messages=messages,
        is_fallback=is_fallback
    )

    response = intents_client.create_intent(
        request={"parent": parent, "intent": intent}
    )

    return dialogflow_dict(response)


def query_training_phrases(intent_id: str, to_list: bool = True):
    """Query all training phrases for Intent ID (assuming GOOGLE_APPLICATION_CREDENTIALS is set as
    .env variable)

    See https://github.com/googleapis/python-dialogflow/blob/main/samples/snippets/list_training_phrases.py for source

    ## Parameters:
    --------------
    * intent_id: unique ID of the intent (not display name, use find_intent for IDs)
    * to_list: if original dialogflow object (False) or a list shuld be returned (True)

    ## Returns:
    -----------
    * If 'to_list' == True: list of phrases

    ## Example:
    ----------
    ```
    import os
    from dialogflow_tools import query_training_phrases

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"/references/credentials/dialogflow_chuck3.json"

    intent_id = 'projects/chuckcopy-jrbd/agent/intents/3d21514f-0a64-40e1-a2d5-362cdbe95ddb'

    phrases = query_training_phrases(intent_id)
    ```
    """
    # Create the intents client
    intent_client = dialogflow.IntentsClient()

    # The options for views of an intent
    intent_view = dialogflow.IntentView.INTENT_VIEW_FULL

    # Compose the get-intent request
    get_intent_request = dialogflow.GetIntentRequest(
        name=intent_id, intent_view=intent_view
    )

    intent = intent_client.get_intent(get_intent_request)
    phrases = intent.training_phrases
    if to_list:
        phrases = ["".join([i.text for i in j.parts]) for j in phrases]
        phrases = list(set(phrases))
    return phrases


def query_intent(intent_id: str, to_dict: bool = True):
    """Query full Intent Information (assuming GOOGLE_APPLICATION_CREDENTIALS is set as .env
    variable)

    See https://github.com/googleapis/python-dialogflow/blob/main/samples/snippets/list_training_phrases.py for source

    ## Parameters:
    --------------
    * intent_id: unique ID of the intent (not display name, use find_intent for IDs)
    * to_dict: if original dialogflow object (False) or a dictionary shuld be returned (True)

    ## Returns:
    -----------
    * If 'to_dict' == True: dictionary of the intent. None, if no such intent

    ## Example:
    ----------
    ```
    import os
    from dialogflow_tools import query_intent

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"/references/credentials/dialogflow_chuck3.json"

    intent_id = 'projects/chuckcopy-jrbd/agent/intents/3d21514f-0a64-40e1-a2d5-362cdbe95ddb'

    intent = query_intent(intent_id)
    ```
    """
    if intent_id is None:
        return None
    # Create the intents client
    intent_client = dialogflow.IntentsClient()

    # The options for views of an intent
    intent_view = dialogflow.IntentView.INTENT_VIEW_FULL

    # Compose the get-intent request
    get_intent_request = dialogflow.GetIntentRequest(
        name=intent_id, intent_view=intent_view
    )

    try:
        intent = intent_client.get_intent(get_intent_request)
    except:
        return None

    if to_dict == False:
        return intent
    else:
        return dialogflow_dict(intent)


def update_intent(
        intent_id: str,
        user_asks: list = [],
        bot_replies: list = [],
        display_name: str = None
    ):
    """Update existing intent directly in dialogflow via API

    See https://stackoverflow.com/questions/55136597/dialogflow-v2-beta-1-update-intent-with-python
    for more info

    Parameters
    ----------
    intent_id: ID of the Bot/Project
    user_asks: list of questions that user would ask
    bot_replies: what should the bot reply. Use multiple lists to allow for random choice of
        replies, see example
    display_name: name of the intent. Do not set (=None) for no change

    Returns
    -------
    updated intent object

    Examples
    ```
    intent_id = 'projects/chuckcopy-jrbd/agent/intents/57dc5572-5f61-4bb6-be78-8cc1bbecd33a'
    user_asks = ['Who is the developer of CaaS system?', 'Can I get the name of the CaaS system developer?']
    bot_replies = [['That guy from Data Analytics', 'I think it is the data scientist from Data Analytics'], 'My advice: ask the other guy from communication department']

    out = update_intent(intent_id,user_asks,bot_replies)
    ```
    """
    intent = query_intent(intent_id, to_dict=False)
    if len(intent.training_phrases) == 0:
        current_training = []
    elif len(intent.training_phrases) == 1:
        current_training = ["".join([i.text.lower() for i in intent.training_phrases[0].parts if i.text])]
    else:
        current_training = [i.parts[0].text.lower() for i in intent.training_phrases]

    to_update = []
    for phrase in user_asks:
        if phrase.lower() not in current_training:
            part = dialogflow.Intent.TrainingPhrase.Part(text=phrase)
            intent.training_phrases.append(dialogflow.Intent.TrainingPhrase(parts=[part]))
            current_training.append(phrase)
            to_update.append("training_phrases")

    current_messages = [i.text.text for i in intent.messages]
    for message in bot_replies:
        message = [message] if isinstance(message, str) else message
        if message not in current_messages:
            part = dialogflow.Intent.Message.Text(text=message)
            intent.messages.append(dialogflow.Intent.Message(text=part))
            to_update.append("messages")

    if display_name is not None and display_name != intent.display_name:
        to_update.append("display_name")
        intent.display_name = display_name

    to_update = list(set(to_update))

    if to_update != []:
        client = dialogflow.IntentsClient()
        client.update_intent(intent=intent,
                             update_mask=FieldMask(paths=to_update))
    return intent
