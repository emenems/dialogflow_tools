import google.cloud.dialogflow_v2 as dialogflow
import json
# import google.protobuf as gpf
import re
import secrets


def get_intent(text: str, bot_id: str, session_id: str, language: str = "en", timeout: int = 10) -> dict:
    """
    Get bot intent/respone providing input "text" (user input), "session_id", ID of the session, and "bot_id" on dialogflow 
    Returns response/message converted to dictionary

    ## Input:
    * text: input text to be send to Dialogflow
    * bot_id: Google Project ID (not dialogflow chatbot name!)
    * sessiont_id: ID of the session to allow for conversation
    * language: input language code (by default 'en')
    * timeout: timeout period in seconds

    ## Example
    ```
    import os
    from dialogflow_tools import get_intent

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"/references/credentials/dialogflow_dev.json"

    text = "Please reserve me a table for 9:30 on 23.8."
    bot_id = "chuckcopy-jrbd"
    session_id = "unique"
    language = "en"
    response_out = get_intent(text, bot_id, session_id, language)
    ```
    """
    # open dialogflow session
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(bot_id, session_id)
    # prepare text query
    text_input = dialogflow.types.TextInput(text=text,language_code=language)
    query_input = dialogflow.types.QueryInput(text=text_input)
    # return complete response (dictionary)
    response = session_client.detect_intent(session=session,query_input=query_input,timeout=timeout)
    return dialogflow_dict(response)


def to_kommunicate(response: dict) -> dict:
    """
    Parse message to kommunicate.io format

    ## Input:
    * response: output of get_intent function (same as dialogflow_tools/tests/_aux_/dialogflow_response.json)

    ## Output:
    * formatted dictionary: https://docs.kommunicate.io/docs/bot-custom-integration
    """
    messages = []
    # get all keys to decide if text or simpleResponse will be used for plain message text
    keys = [i for d in response["queryResult"]["fulfillmentMessages"] for i in d.keys()]
    plain_text = "simpleResponses" if "simpleResponses" in keys else "text"
    # extract entries one after anothre
    for i in response["queryResult"]["fulfillmentMessages"]:
        # start with simple text
        if plain_text in i.keys():
            out_text = i[plain_text][plain_text][0]
            out_text = out_text['textToSpeech'] if plain_text != "text" else out_text
            messages.append({"message": out_text})
        # add buttons
        elif "suggestions" in i.keys():
            payload = []
            for j in i["suggestions"]["suggestions"]:
                payload.append({"title":j["title"],
                                "message":j["title"]})
            messages.append(
                            {"message":"",
                             "metadata": {
                                          "contentType": "300",
                                          "templateId": "6",
                                          "payload": payload
                                         }
                            }
                           )
        # add (external) links
        elif "linkOutSuggestion" in i.keys():
            messages.append(
                            {"message":"",
                             "metadata": {
                                          "contentType": "300",
                                          "templateId": "3",
                                          "payload": [
                                                      {
                                                      "type":"link",
                                                      "url":i["linkOutSuggestion"]["uri"],
                                                      "name":i["linkOutSuggestion"]["destinationName"]
                                                      }
                                                     ]
                                         }
                            }
                           )
    return messages


def from_kommunicate(message: dict) -> dict:
    """
    Parse message from kommunicate and konvert to own format (see Output help)

    ## Input
    * message: kommunicate.io formatted dictionary (https://docs.kommunicate.io/docs/bot-custom-integration)

    ## Output dictonary:
    * "sessionId": conversation id
    * "eventName": events e.g. WELCOME
    * "message": message sent by user to the bot

    """
    return {"sessionId": message["groupId"],
            "eventName": message["eventName"],
            "message": message["message"]}


def to_custom_dtse(response: dict, session_id: str = None) -> dict:
    """
    Parse message to custom DTSE CZ chat-bot front-end format

    ## Input:
    * response: output of get_intent function (same as dialogflow_tools/tests/_aux_/dialogflow_response.json = https://cloud.google.com/dialogflow/es/docs/reference/rest/v2/DetectIntentResponse)
    """
    if session_id == None:
        session_id = re.findall("sessions\/(.*)\/contexts",
                                response["queryResult"]["outputContexts"][0]["name"])[0]
    
    messages = ""
    # get all keys to decide if text or simpleResponse will be used for plain message text
    keys = [i for d in response["queryResult"]["fulfillmentMessages"] for i in d.keys()]
    plain_text = "simpleResponses" if "simpleResponses" in keys else "text"
    # extract entries one after anothre
    for i in response["queryResult"]["fulfillmentMessages"]:
        # start with simple text
        if plain_text in i.keys():
            out_text = i[plain_text][plain_text][0]
            out_text = out_text['textToSpeech'] if plain_text != "text" else out_text
            messages += f"</br>{out_text}" if messages != "" else f"{out_text}"
    return {"fulfilment":messages,
            "sessionID":session_id}


def from_custom_dtse(message: dict) -> dict:
    """
    Parse message from custom DTSE CZ chat-bot front-end

    ## Input
    * message: dictionary containing message + session_id

    ## Output dictonary:
    * "sessionId": conversation id
    * "message": message sent by user to the bot

    """
    if "session_id" not in message.keys():
        return {"sessionId": secrets.token_hex(24),"message":message["message"]}
    elif message["session_id"] == None or message["session_id"] == 'None':
        return {"sessionId": secrets.token_hex(24),"message":message["message"]}
    else:
        return {"sessionId": message["session_id"],"message": message["message"]}


def dialogflow_dict(response, camel_case: bool = True) -> dict:
    """Aux function to convert google.cloud.dialogflow_v2.types.session.DetectIntentResponse to dictionary
    
    Previous version worked with google.protobuf.json_format.MessageToDict(response) but this is no longer valid with update to v2

    Parameters:
    -----------
    response: output of 'detect_intent' method
    camel_case: if the output keys should be used as 'Camel-cased', i.e. if response_id should be converted to responseId

    Returns:
    --------
    response as dictionary
    """
    if camel_case == True:
        return json.loads(type(response).to_json(response))
    else:
        return type(response).to_dict(response)
