from dialogflow_tools import message_io
import json


def test_to_kommunicate():
    with open("dialogflow_tools/tests/_aux_/dialogflow_response.json","r") as fid:
        response = json.load(fid)

    out = message_io.to_kommunicate(response)

    assert out == [{"message":"Pre kolko osob?"},
                   {"message":"",
                    "metadata":{"contentType":"300",
                                "templateId":"6",
                                "payload": [{"title": "1", "message": "1"}, {"title": "viac", "message": "viac"}]}}]


def test_from_kommunicate():
    with open("dialogflow_tools/tests/_aux_/kommunicate_sends.json","r") as fid:
        response = json.load(fid)

    out = message_io.from_kommunicate(response)

    assert out == {"sessionId":"conversation id",
                   "eventName": "events ie. WELCOME , KOMMUNICATE_MEDIA_EVENT etc",
                   "message": "message sent by user to the bot"}


def test_to_custom_dtse():
    with open("dialogflow_tools/tests/_aux_/dialogflow_response.json","r") as fid:
        response = json.load(fid)

    out = message_io.to_custom_dtse(response)

    assert out == {"fulfilment":"Pre kolko osob?","sessionID":"8607badf-4bae-5cd4-eeb0-d11dc2116e91"}


def test_from_custom_dtse():
    with open("dialogflow_tools/tests/_aux_/custom_dtse_sends.json","r") as fid:
        response = json.load(fid)

    out = message_io.from_custom_dtse(response)

    assert out == {"sessionId":12345,
                   "message": "message sent by user to the bot"}

    out = message_io.from_custom_dtse({"message":"second test"})
    assert out["message"] == "second test"
    assert "sessionId" in out.keys()
