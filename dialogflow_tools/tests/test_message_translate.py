from dialogflow_tools import message_translate
import os
import json 


def test_correct_message_time():
	# read dict with message + convert to message format
    if os.path.isfile("dialogflow_tools/tests/_aux_/dialogflow_credentials_translate_valid.json"):
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"dialogflow_tools/tests/_aux_/dialogflow_credentials_valid.json"
    
        out = message_translate.translate_gcp(["Ahoj, preloz to prosim"],
                                          language_list = ["sk","cz"],
                                          target_language="en")

        assert "translate" in out[0]

