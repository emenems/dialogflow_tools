from dialogflow_tools import import_export
import json
import os

# Will carry out tests only if this file exists
CREDENTIALS = "dialogflow_tools/tests/_aux_/dialogflow_credentials_valid.json"


def test_create_intent_json():
    out = import_export.create_intent_json("First test",
                                           ["Test1", "Test2"],
                                           None,
                                           is_fallback=True)
    assert out.get("name") == "First test"
    assert out.get("userSays") == [{'isTemplate': False, 'data': [{'text': 'Test1', 'userDefined': False}], 'count': 0, 'updated': None}, {'isTemplate': False, 'data': [{'text': 'Test2', 'userDefined': False}], 'count': 0, 'updated': None}]
    assert out.get("responses")[0].get("messages") == [{"type": "message", "condition": "", "speech": []}]
    assert out.get("fallbackIntent") is True

    out = import_export.create_intent_json("Second test",
                                           ["Test1", "Test2"],
                                           [["Row1_option1", "Row1_option2"], "Row2"])
    assert out.get("name") == "Second test"
    assert out.get("userSays") == [{'isTemplate': False, 'data': [{'text': 'Test1', 'userDefined': False}], 'count': 0, 'updated': None}, {'isTemplate': False, 'data': [{'text': 'Test2', 'userDefined': False}], 'count': 0, 'updated': None}]
    assert out.get("responses")[0].get("messages") == [{'type': 'message', 'condition': '', 'speech': ['Row1_option1', 'Row1_option2']}, {'type': 'message', 'condition': '', 'speech': ['Row2']}]

    out_file = "dialogflow_tools/tests/_aux_/test_import_output_create_intent.json"
    if os.path.isfile(out_file):
        os.remove(out_file)
    outX = import_export.create_intent_json("delete please",
                                           ["Test1","Test2"],
                                           [["Row1_option1","Row1_option2"],"Row2"],
                                           out_file)
    assert os.path.isfile(out_file)
    with open(out_file, "r") as fid:
        outX = json.load(fid)
    assert outX.get("name") == "delete please"
    assert outX.get("fallbackIntent") is False
    os.remove(out_file)


def get_credentials():
    if os.path.isfile(CREDENTIALS):
        with open(CREDENTIALS, "r") as fid:
            out = json.load(fid)
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = CREDENTIALS
        return out["project_id"]
    else:
        None

def test_query_intents():
    bot_id = get_credentials()
    if bot_id:
        out = import_export.query_intents(bot_id, to_dict=False)
        assert "google.cloud.dialogflow" in str(type(out)).lower()
        # the part with conversion to dicy will be applied in test_find_intent

        out = import_export.query_intent(None, to_dict=False)
        assert out is None

        out = import_export.query_intent("project/does/no/exist", to_dict=True)
        assert out is None
    else:
        pass


def test_find_intent_and_training_phrases():
    bot_id = get_credentials()
    if bot_id:
        out_file = "dialogflow_tools/tests/_aux_/test_find_intent.json"
        if os.path.isfile(out_file):
            os.remove(out_file)
        ids = import_export.query_intents(bot_id, to_dict=True)
        out = import_export.find_intent(ids,
                                        intent_name=ids[list(ids.keys())[1]]["displayName"],
                                        use_display_name=True,
                                        return_intent=False,
                                        output_file=out_file)
        assert os.path.isfile(out_file)
        assert out == list(ids.keys())[1]
        os.remove(out_file)

        out = import_export.query_training_phrases(list(ids.keys())[0], to_list=True)
        assert isinstance(out, list)
    else:
        pass


def test_create_intent_and_delete_intent():
    bot_id = get_credentials()
    if bot_id:
        out = import_export.create_intent(bot_id,
                                          display_name="TEST: unit test to be deleted",
                                          user_asks=["Does Data Analytics team do unit testing?","Is unit testing a standard in data analytics team?"],
                                          bot_replies=["Unfortunately, only few unit tests are done in Data Analytics team"])
        assert out["displayName"] == "TEST: unit test to be deleted"

        out = import_export.delete_intent(out["name"])
        assert out == True
    else:
        pass
