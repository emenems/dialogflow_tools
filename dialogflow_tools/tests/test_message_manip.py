from dialogflow_tools import message_manip
import json

try:
    import calendar_tools
    module_exists = True
except ModuleNotFoundError:
    module_exists = False

def test_correct_message_time():
    if module_exists:
        # read dict with message + convert to message format
        with open("dialogflow_tools/tests/_aux_/dialogflow_response.json","r") as fid:
            response_orig = json.load(fid)
        
        response_new = message_manip.correct_message_datetime(response_orig,
                                                              date_field="eu_date",
                                                              time_field="time")

        for i in range(len(response_orig["queryResult"]["outputContexts"])):
            assert response_new["queryResult"]["outputContexts"][i]["parameters"]["time"] == "2020-02-16T09:30:00+01:00"
            assert response_new["queryResult"]["outputContexts"][i]["parameters"]["time.original"] == "9:30"
            assert response_new["queryResult"]["outputContexts"][i]["parameters"]["eu_date"][5:10] == "08-23"
            assert response_new["queryResult"]["outputContexts"][i]["parameters"]["eu_date.original"] == "23.8."
