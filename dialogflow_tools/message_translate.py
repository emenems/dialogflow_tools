from google.cloud import translate_v2
import numpy as np
from scipy.stats import mode


def translate_gcp(text, language_list: list = ["sk","cz"], target_language: str = "en"):
    """
    Translate text using google translator and language detection

    ## Input
    * text: input text to be translated. Expecting list ["message1","message2"]
    * language_list: constrain/bounds for language detection. If detected lagnuage not in list, will set to first value
    * target_language: output language

    ## Output:
    * list of translated text

    ## Example
    ```
    import os
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"d:/projekt/credentials/demochatbots-1efee945dc06.json"

    from dialogflow_tools import translate_gcp
    out = translate_gcp(["Ahoj, preloz to prosim"],language_list = ["sk","cz"])
    ```
    """
    # expecting list
    if not isinstance(text, list):
        text = [text]
    # open client
    translate_client = translate_v2.Client()
    # detect language = process all sentences and use the most frequent one
    language_detected = np.array([])
    for i in text:
        language_detected = np.append(language_detected,translate_client.detect_language(i)["language"])
    use_language = mode(language_detected)[0][0]
    # use source language if within given list. otherwise, use first entry
    if use_language in language_list:
        apply_fce = lambda x: translate_client.translate(x,target_language="en",
                                                        source_language=use_language)
    else:
        apply_fce = lambda x: translate_client.translate(x,target_language="en",
                                                        source_language=language_list[0])                                                    
    # translate all text entries
    translated = []
    for i in text:
        translated.append(apply_fce(i)["translatedText"])
    return translated

